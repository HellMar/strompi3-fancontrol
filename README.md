# StromPI3 FanControl
Dieses Tool regelt die Lüftergeschwindigkeit eines StromPi3 Gehäuses  [StromPi3 Gehäuse](https://joy-it.net/de/products/RB-StromPi3-Case) anhand der CPUTemperatur.

## Abhängigkeiten
Das Tool läuft ausschließlich mit **Python >= 3**.
Folgende (Python-)Abhängigkeiten werden benötigt:

- [gpiozero](https://gpiozero.readthedocs.io/en/stable/)
```
    pip3 install gpiozero
```

## Installation
##### 1. Dieses Repo Clonen:
    git clone https://gitlab.com/HellMar/strompi3-fancontrol.git /opt/strompi3-fancontrol
##### 2. Systemd file "Installieren":
    cp /opt/strompi3-fancontrol/strompi3-fancontrol.service /lib/systemd/system/strompi3-fancontrol.service
##### 3. Starten:
    systemctl start strompi3-fancontrol.service
##### 4. (Optional) Automatischer start nach dem Booten aktivieren:
    systemctl enable strompi3-fancontrol.service

## Konfiguration
Die Konfiguration erfolgt über die Konfigurationsdatei config.json im JSON Format.

#### Debug
    "debug" : false
Durch aktivierung werden diverse Meldungen zu Fehlersuche auf der Konsole ausgegeben. 

#### starttemp
    "starttemp" : 55
Temperatur bei derer überschreitung der Lüfter Startet.

#### pwmpin
    "pwmpin" : 2
Legt den Pin am RasPi fest an dem der Lüfter angeschlossen ist. 

#### controller
```
"controller" : {
    "proportional" : 4,
    "integral" : 0.2
}
```
Proportional und Integralanteil der Regelung.

---
**NOTE**

Basiert auf dem Script aus der Anleitung zum [SromPi3-Case](https://joy-it.net/files/files/Produkte/RB-StromPi3-Case/Anleitung-RB-StromPi3-Case-2020-10-02.pdf)

---
 